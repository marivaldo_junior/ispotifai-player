import React from "react";
import { Link } from "react-router-dom";

export default function Home() {
  return (
    <div className="home-container">
      <div className="home-left">
        <div className="home-title">Ispotifai Player</div>
        <div className="home-desc">
          Um simples app de streaming de musica com React, Redux and
          Deezer API
        </div>
        <Link to="/player/albums">
          <button className="custom-button">Comece a ouvir</button>
        </Link>

      </div>
    </div>
  );
}
